FROM python:3.8
ENTRYPOINT ["bash","entrypoint.prod.sh"]

# copy project
COPY . .

EXPOSE 8000

RUN apt-get update -y && \
    apt-get install -y nginx uwsgi \
                    uwsgi-plugin-python3 gcc \
                    systemctl

RUN pip install --upgrade pip &&\
    pip install -r requirements.txt



