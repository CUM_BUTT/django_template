#!/bin/bash

python app/manage.py collectstatic --noinput

# works with UWSGI
ln -s /app/bash_scripts/confs/nginx.conf /etc/nginx/sites-enabled/
ln -s /app/bash_scripts/confs/uwsgi.ini /etc/uwsgi/apps-available/
ln -s /app/bash_scripts/confs/uwsgi.ini /etc/uwsgi/apps-enabled/


service uwsgi restart

/etc/init.d/nginx restart
nginx -t # check is nginx ok

uwsgi --ini /app/bash_scripts/confs/uwsgi.ini

SOCKET=/app/app/ridt/ridt.sock
cd /app/app && uwsgi --socket $SOCKET --module ridt.wsgi --chmod-socket=664
